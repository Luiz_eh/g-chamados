import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { Chamado } from "src/app/model/chamado.model";
import { DefaultResponse } from "src/app/model/response.model";

const URL_BASE = "http://localhost:8080/    "
const URL_FIND_BY_RESPONSAVEL = URL_BASE + "/  "
const URL_FIND_BY_NOME_CHAMADO = URL_BASE + "/   /"

@Injectable({
    providedIn: 'root'
})
export class ChamadoService {

    constructor(private http: HttpClient) {

    }

    findAll(): Observable< DefaultResponse<Chamado[]>>{
        return this.http.get< DefaultResponse<Chamado[]>>(URL_BASE)
    }

    findByResponsavel(responsavel: string): Observable< DefaultResponse<Chamado[]>> {
        return this.http.get< DefaultResponse<Chamado[]> >(URL_FIND_BY_RESPONSAVEL + responsavel)
    }
   

}