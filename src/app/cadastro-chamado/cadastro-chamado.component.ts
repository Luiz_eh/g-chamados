import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { RegistroChamados } from '../model/chamado.model';

@Component({
  selector: 'app-cadastro-chamado',
  templateUrl: './cadastro-chamado.component.html',
  styleUrls: ['./cadastro-chamado.component.css']
})
export class CadastroChamadoComponent {


  chamadoForm = this.formBuilder.group({
    name_issuer: ['', [Validators.required]],
    email_issuer: ['', [Validators.required, Validators.email]],
    name_responder: ['', [Validators.required]],
    email_responder: ['', [Validators.required, Validators.email]],
    title: ['', [Validators.required]],
    description: ['', [Validators.required]]

  })

  constructor(private formBuilder: FormBuilder,
              private router: Router) { }


  cadastrar() {
      let chamados = new RegistroChamados(
        this.name_issuer.value,
        this.email_issuer.value,
        this.name_responder.value,
        this.email_responder.value,
        this.title.value,
        this.description.value
    )

    
      

  }
  returnToList() {
    this.router.navigateByUrl('/listagemChamados')
  }




  get name_issuer() {
    return this.chamadoForm.controls.name_issuer
  }
  get email_issuer() {
    return this.chamadoForm.controls.email_issuer
  }
  get name_responder() {
    return this.chamadoForm.controls.name_responder
  }
  get email_responder() {
    return this.chamadoForm.controls.email_responder
  }
  get title() {
    return this.chamadoForm.controls.title
  }
  get description() {
    return this.chamadoForm.controls.description
  }


 


}
