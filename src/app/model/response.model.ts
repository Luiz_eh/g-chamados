export class DefaultResponse<C> { 
    public message: string  
    public result: C  
}