import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { ChamadoService } from 'service/chamado.service';
import { Chamado } from '../model/chamado.model';
import { DefaultResponse } from '../model/response.model';

@Component({
  selector: 'app-lista-chamados',
  templateUrl: './lista-chamados.component.html',
  styleUrls: ['./lista-chamados.component.css']
})
export class ListaChamadosComponent implements OnInit {



  chamados: Chamado[] = []

  formSearch = this.formBuilder.group ({
    searchType: [1, [Validators.required]],
    searchText: ['', [Validators.required]]
  })

  constructor(private formBuilder: FormBuilder,
              private chamadoService: ChamadoService) { }


  ngOnInit() {
    this.findChamados
  }


  findChamados(){
    this.chamadoService.findAll().subscribe(response => this.onSearchResult(response))
  }

  search() {
    if (this.searchType.value == 1) {
      this.chamadoService.findByResponsavel(this.searchText.value).subscribe(response => this.onSearchResult(response))
    }
  }

  onSearchResult(response: DefaultResponse<Chamado[]>) {
    if (response != null) {
      this.chamados = response.result
    }
  }


  clearSearch() {
    this.searchText.setValue("")
    this.searchType.setValue(1)
  }



  get searchType() {
    return this.formSearch.controls.searchType
  }
  get searchText() {
    return this.formSearch.controls.searchText
  }



  atualizaStatus() {
    if (status == '1') {
        status = '0'
    } 
    if (status == '0') {
        status = '2'
    }
  }
 

}
