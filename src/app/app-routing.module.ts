import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CadastroChamadoComponent } from './cadastro-chamado/cadastro-chamado.component';
import { ListaChamadosComponent } from './lista-chamados/lista-chamados.component';
import { ListaDetalhesComponent } from './lista-detalhes/lista-detalhes.component';

const routes: Routes = [
    { path: 'cadastroChamado', component: CadastroChamadoComponent},
    { path: 'listagemChamados', component: ListaChamadosComponent},
    { path: 'listaDetalhes', component: ListaDetalhesComponent},
    { path: '', pathMatch: 'full', redirectTo: '/cadastroChamado'}, //Problema
    
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
