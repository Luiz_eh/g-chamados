import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';

import { HttpClientModule } from "@angular/common/http";

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CadastroChamadoComponent } from './cadastro-chamado/cadastro-chamado.component';
import { ListaChamadosComponent } from './lista-chamados/lista-chamados.component';
import { ListaDetalhesComponent } from './lista-detalhes/lista-detalhes.component';

@NgModule({
  declarations: [
    AppComponent,
    CadastroChamadoComponent,
    ListaChamadosComponent,
    ListaDetalhesComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
